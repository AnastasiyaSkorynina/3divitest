﻿//Приложение-сервис, которое детектирует лица людей на видеопотоке с подключенной веб-камеры и позволяет получить результаты детекции через http-запрос

#include "opencv2/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"
#include "SimpleWebServer/server_http.hpp"
#include <iterator>
#include <iostream>

using namespace cv;
using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

CascadeClassifier face_cascade;//классификатор для детекции лиц
VideoCapture cam(0);//открываем камеру

enum ErrorCode //коды ошибок
{
	ERR_NO_ERROR = 0,
	ERR_NO_FILE = 1,
	ERR_NO_CAMERA = 2,
	ERR_INTERNAL_ERROR = 99
};

/**
 * получение строки в формате JSON из заданного вектора детектированных лиц
 * @param facerect вектор детектированных лиц
 * @see find_faces()
 * @return Строка в формате JSON
 */
std::string print_json_result(const std::vector <Rect> &facerect)
{
	std::stringstream result;
	result << "{[";
	for (auto it = facerect.begin(); it != facerect.end(); ++it) 
	{
		if (it != facerect.begin()) result << ", ";
		result << "{";
		result << "\"x\": " << it->x << ", ";
		result << "\"y\": " << it->y << ", ";
		result << "\"width\": " << it->width << ", ";
		result << "\"height\": " << it->height;
		result << "}";
	}
	result << "]}";
	return result.str();
}

/**
 * отрисовка прямоугольников по списку детектированных лиц на заданном фрейме
 * @param frame текущий фрейм, на котором необходимо произвести отрисовку
 * @param facerect вектор детектированных лиц
 * @return Фрейм с отрисованными прямоугольниками
 */
Mat print_image_result(const Mat &frame, const std::vector <Rect> &facerect)
{
	Mat result = frame.clone();
	// отрисовываем прямоугольники лиц
	for (const auto &rect : facerect)
	{
		rectangle(result, rect, Scalar(0, 255, 0));
	}
	return result;
}

/**
* запись данных из Mat в буфер в формате *.jpg
* @param frame текущий фрейм
* @param buf буфер
* @return Код ошибки (см. ErrorCode)
*/

int get_image(const Mat& frame, std::vector<uchar>& buf)
{
	if (imencode(".jpg", frame, buf)) 
		return ERR_NO_ERROR;
	else 
		return ERR_INTERNAL_ERROR;
}

/**
* нахождение лиц на фрейме frame и запись результата в faces
* @param frame текущий фрейм
* @param faces вектор детектированных лиц
* @return Код ошибки (см. ErrorCode)
*/
int find_faces(Mat& frame, std::vector < Rect > & faces)
{
	if (cam.isOpened())//проверяем, что камера доступна
	{
		cam >> frame; // получаем кадр
		//сделаем изображение черно-белым для работы алгоритма
		Mat gray;
		cvtColor(frame, gray, COLOR_BGR2GRAY);
		face_cascade.detectMultiScale(gray, faces);//находим лица
	}
	else 
		return ERR_NO_CAMERA;
	return ERR_NO_ERROR;
}

int main()
{
	//инициализируем классификатор
	bool res_load = 0;
	res_load = face_cascade.load("haarcascade_frontalface_default.xml");
	if (!res_load)
	{
		std::cerr << "ERROR: Can't load xml for face detection" << std::endl;
		return ERR_NO_FILE;
	}
	if (!cam.isOpened())
	{
		std::cerr << "ERROR: Can't initialize camera capture" << std::endl;
		return ERR_NO_CAMERA;
	}
	//конфигурируем сервер
	HttpServer server;
	server.config.port = 8080;
	server.resource["^/getResult$"]["GET"] = [](std::shared_ptr<HttpServer::Response> response, std::shared_ptr<HttpServer::Request> request) 
	{
		//проверяем параметры запроса на соответствие ожидаемым
		auto params = request->parse_query_string();
		auto typeIt = params.find("type");
		if (typeIt == params.end()) 
		{
			std::cerr << "ERROR: Bad request: no type parameter" << std::endl;
			response->write(SimpleWeb::StatusCode::client_error_bad_request);
			return;
		}
		auto result_type = typeIt->second;
		if (result_type != "json" && result_type != "image")
		{
			std::cerr << "ERROR: Unexpected type: " << result_type << std::endl;
			response->write(SimpleWeb::StatusCode::client_error_bad_request);
			return;
		}
		//выполняем детектирвоание лиц на текущем фрейме
		Mat frame;
		Mat result_frame;
		std::vector < Rect > faces;
		int err = find_faces(frame, faces);
		if (err == ERR_NO_CAMERA)
		{
			std::cerr << "ERROR: Can't initialize camera capture" << std::endl;
			response->write(SimpleWeb::StatusCode::server_error_service_unavailable, "Can't initialize camera capture");
			return;
		}
		//если запрошен тип json, выдаём результат в формате JSON
		if (result_type == "json")
		{
			response->write(print_json_result(faces));
		}
		//если запрошен тип image, выдаём результат в формате jpeg
		else if (result_type == "image")
		{
			SimpleWeb::CaseInsensitiveMultimap header;
			result_frame = print_image_result(frame, faces);
			std::vector<uchar> buf_result;
			if (get_image(result_frame, buf_result) != ERR_NO_ERROR) 
			{
				response->write(SimpleWeb::StatusCode::server_error_internal_server_error);
				return;
			}
			header.emplace("Content-Length", std::to_string(buf_result.size()));
			response->write(header);
			response->write(reinterpret_cast<char*>(buf_result.data()), buf_result.size());	
		}
	};
	//при запросах, не соответствующих ожидаемым, выдаём сообщение об ошибке
	server.default_resource["GET"] = [](std::shared_ptr<HttpServer::Response> response, std::shared_ptr<HttpServer::Request> request) 
	{
		std::cerr << "ERROR: unexpected request " << request->path << std::endl;
		response->write(SimpleWeb::StatusCode::client_error_not_found);
	};
	//запускаем сервер
	server.start();
	return ERR_NO_ERROR;
};


